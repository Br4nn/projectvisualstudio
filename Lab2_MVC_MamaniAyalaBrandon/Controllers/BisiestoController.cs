﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lab2_MVC_MamaniAyalaBrandon.Models;

namespace Lab2_MVC_MamaniAyalaBrandon.Controllers
{
    public class BisiestoController : Controller
    {
        // GET: Bisiesto
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Mostrar(ClsBisiesto ObjBisiesto) {

            ObjBisiesto.Bisiesto = " ";
            ObjBisiesto.Nobisiesto = " ";
            ObjBisiesto.Resultado = " ";
            ObjBisiesto.Inicio = Convert.ToInt32(Request.Form["Inicio"]);
            ObjBisiesto.Fin = Convert.ToInt32(Request.Form["Fin"]);

            for (int i = ObjBisiesto.Inicio; i <= ObjBisiesto.Fin; i++) {
                ObjBisiesto.Todo += i + ", ";
                if (i % 400 == 0)
                {
                    ObjBisiesto.Bisiesto = ObjBisiesto.Bisiesto + i + "\n";
                }
                else if (i % 4 == 0 && i % 400 == 0 && i % 100 != 0)
                {
                    ObjBisiesto.Bisiesto = i + "\n";
                }
                else
                {
                    ObjBisiesto.Nobisiesto += i + "\n";
                }
            }
            string Tipo = Request.Form["Anio"];
            if (Tipo == "Bisiestos") { ObjBisiesto.Resultado = ObjBisiesto.Bisiesto; }
            else if (Tipo == "No Bisiestos") { ObjBisiesto.Resultado = ObjBisiesto.Nobisiesto; }
            else if (Tipo == "Ambos") { ObjBisiesto.Resultado = ObjBisiesto.Todo; }
            
            
            return View(ObjBisiesto);
        }
    }
}